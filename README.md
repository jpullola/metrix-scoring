# Metrix Scoring

Metrix scoring on Pyhton Tkinter -pohjainen sovellus, joka on suunniteltu käytettäväksi frisbeegolfradalla tulosten kirjaamiseen. Sovelluksessa on tällä hetkellä 13 rataa ratakarttoineen ja osalla radoista myös kuvat jokaisesta väylästä. Sovelluksessa on yksinkertainen Tkinter -kirjastolla suunniteltu käyttöliittymä. Käyttäjä voi jokaisen heiton jälkeen syöttää heiton laskeutumispaikan käyttämällä alalaidassa olevia painikkeita. Sovellus laskee tämän perusteella automaattisesti kierroksen tuloksen ja muita tilastotietoja kuten puttien onnistumisprosentin, rangaistukset ja avausten tarkkuuden. Jokaisen kierroksen tulokset tallennetaan omaan tiedostoonsa, jonka nimessä on radan nimi ja kierroksen aloitusaika. Tulostiedosto on suunniteltu siten, että siitä on helppo kopioida tulokset Excel -taulukkoon, jossa on edistyneemmät työkalut tilastojen tarkastelemiseen.

##### Huom: 
Sovellusta on testattu vain OnePlus 5 ja 3:lla (5,5" 1920x1080). Muunkokoisilla/-resoluutioisilla näytöillä sovelluksen ulkoasu saattaa näyttää sekaiselta.

## Sovelluksen käynnistäminen

Lataa sovellus Python -skriptien ajamista varten, esimerkiksi Pydroid 3: https://play.google.com/store/apps/details?id=ru.iiec.pydroid3&hl=fi 

Lataa tiedostot laitteellesi, avaa dgmtrx.py Python -sovelluksella ja suorita se. 


## Sovelluksen käyttö

Valitse aluksi rata painamalla "Valitse rata" ja valitsemalla listalta rata. Paina "Aloita kierros". (screenshot1.png)

Merkitse jokaisen heiton laskeutumispaikka käyttämällä alalaidassa olevia painikkeita (selitetty edempänä). Painikkeiden yläpuolella on painikkeet edelliselle/seuraavalle väylälle siirtymistä varten. Näiden yläpuolella on kuva väylästä jos sellainen on saatavilla. Kuvan yläpuolella on tekstikenttä, johon jokaisen heiton tulos tulostuu (selitetty painikkeiden selityksen yhteydessä). Tekstikentän yläpuolella on tiedot väylästä tulokset. Tämän yläpuolella on painikkeet kierroksen lopettamiselle, edellisen heiton poistamiselle, radan vaihtamiselle ja tarkempien tulosten tarkastelulle.  (screenshot2.png)

Tulosten tarkasteluruudun alalaidassa on taulukko, jossa on tiedot kierroksen tuloksista (tilastoja selitetty alempana). Ylälaidassa on painikkeet kierroksen lopettamiselle, palaamiselle takaisin tulosten kirjaamiseen ja tulosten palauttamiseen tallennetusta varmuuskopiosta (Huom! Varmuuskopiotiedostoon kirjoitetaan uuden kierroksen tiedot kun heittojen yhteismäärä on vähintään 3. Edellisen kierroksen tiedot on mahdollista palauttaa ennen tätä).  (screenshot3.png)

Painikkeiden selitykset:

  - Kori: Kiekko laskeutui koriin ilman rangaistuksia, "k"
  - Väylä: Kiekko laskeutui väylälle ilman rangaistuksia, yli 10m päähän korista, "v"
  - OB: Kiekko laskeutui OB-alueelle (tai väylälle rangaistuksen kera, esim. foot fault, mando miss), "o"
  - 3-10m: Kiekko laskeutu 3-10m päähän korista ilman rangaistuksia, "g"
  - 3m: Kiekko laskeutui alle 3m päähän korista ilman rangaistuksia, "b"
  - Green (PEN): Kiekko laskeutui 3-10m päähän korista rangaistuksen kera (OB, foot fault yms.), "G"
  - BUE (PEN): Kiekko laskeutui alle 3m päähän korista rangaistuksen kera (OB, foot fault yms.), "B"
  - Kori (PEN): Kiekko laskeutui koriin rangaistuksen kera (foot fault, mando miss yms.), "K"
 
Tilastojen selitykset:

  - BUE: Pelaaja pääsee alle 3 metriin kaksi heittoa väylän par lukemaa pienemmällä heittomäärällä, ts. birdie-putti alle 3 metristä
  - GH: Pelaaja pääsee alle 3-10 metriin kaksi heittoa väylän par lukemaa pienemmällä heittomäärällä, ts. birdie-putti 3-10 metristä
  - OCP: Onnistuneet putit yli 10 metristä
  - ICP: Putit 3-10 metristä, epäonnistuneet putit alle 3 metristä
  - IBP: Onnistuneet putit alle 3 metristä
  - OB: Rangaistukset (out-of-bounds, foot fault, mando miss yms.)
 

## Tulosten kopiointi Excel -taulukkoon

Kopioi tulostiedoston sisältö leikepöydälle ja liitä se Excel -taulukon oikeaan soluun (kaavioiden jälkeen on sitä varten taulukoita tätä varten, riviltä 132 lähtien). Taulukossa on valmiiksi luotuna taulut kuutta rataa varten. Syötä radan tiedot soluihin B1-S3. Taulukoiden kaavioissa on tietoa radan tuloksista ja kierrosten tilastoista. 


## Mitä opin?

Opin paljon Tkinter -kirjaston käytöstä, kuinka sillä voi luoda yksinkertaisen käyttöliittymän ja miten käyttöliittymään voidaan lisätä kuvia. Opin myös tapoja esittää reaalimaailman tapahtumia Pythonin listojen avulla ja käsitellä raakaa dataa niin että siitä saadaan muokattua käyttäjälle hyödyllisiä tietoja.