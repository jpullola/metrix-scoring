from tkinter import *
import tkinter.messagebox
import time

root = Tk()
root.columnconfigure(0, weight=1)
root.rowconfigure(2, weight=1)



course = "Esimerkkirata"
numOfHoles = 6
holes = list(range(1, numOfHoles+1))
length = ["97m", "116m", "270m", "179m", "102m", "95m"]
par = [3, 3, 5, 4, 3, 3]
throws = [[] for _ in range(numOfHoles)]

courseFolder = "Courses"
courseMapImage = PhotoImage(file=courseFolder + "/selectCourse.png")
holePhotosAvailable = 0

def selectedCourse(x):
    global course, numOfHoles, length, par, throws, holes, courseMapImage, courseMapLabel, courseFolder, holePhotosAvailable

    if x == 1:
        course = "Meri-Toppila"
        numOfHoles = 18
        holes = list(range(1, numOfHoles + 1))
        length = ["109m", "89m", "110m", "60m", "71m", "125m", "86m", "110m", "81m", "129m", "160m", "88m", "135m", "86m", "90m", "70m", "95m", "75m"]
        par = [3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 4, 3, 3, 3, 3, 3, 3, 3]
        courseFolder = "Courses/Meri-Toppila"
        courseMapImage = PhotoImage(file=courseFolder + "/coursemap.png")
        courseMapLabel.config(image=courseMapImage)
        holePhotosAvailable = 1
        if any(throws):
            roundStarted = tkinter.messagebox.askquestion("Radan vaihto", "Radan vaihtaminen poistaa \naloitetun kierroksen tiedot. \nHaluatko varmasti vaihtaa rataa?")
            if roundStarted == "yes":
                throws = [[] for _ in range(numOfHoles)]
        else:
            throws = [[] for _ in range(numOfHoles)]

    if x == 2:
        course = "Virpiniemi"
        numOfHoles = 18
        holes = list(range(1, numOfHoles + 1))
        length = ["70m", "110m", "111m", "88m", "78m", "211m", "109m", "95m", "167m", "74m", "256m", "99m", "206m", "150m", "226m", "116m", "138m", "96m"]
        par = [3, 3, 3, 3, 3, 5, 3, 3, 4, 3, 5, 3, 4, 4, 5, 3, 3, 3]
        courseFolder = "Courses/Virpiniemi"
        courseMapImage = PhotoImage(file=courseFolder + "/coursemap.png")
        courseMapLabel.config(image=courseMapImage)
        holePhotosAvailable = 1
        if any(throws):
            roundStarted = tkinter.messagebox.askquestion("Radan vaihto", "Radan vaihtaminen poistaa \naloitetun kierroksen tiedot. \nHaluatko varmasti vaihtaa rataa?")
            if roundStarted == "yes":
                throws = [[] for _ in range(numOfHoles)]
        else:
            throws = [[] for _ in range(numOfHoles)]

    if x == 3:
        course = "Hiironen"
        numOfHoles = 18
        holes = list(range(1, numOfHoles + 1))
        length = ["90m", "74m", "99m", "243m", "79m", "80m", "197m", "121m", "133m", "123m", "69m", "77m", "113m", "98m", "113m", "99m", "87m", "104m"]
        par = [3, 3, 3, 5, 3, 3, 4, 3, 3, 3, 3, 3, 4, 3, 3, 3, 3, 3]
        courseFolder = "Courses/Hiironen"
        courseMapImage = PhotoImage(file=courseFolder + "/coursemap.png")
        courseMapLabel.config(image=courseMapImage)
        holePhotosAvailable = 1
        if any(throws):
            roundStarted = tkinter.messagebox.askquestion("Radan vaihto", "Radan vaihtaminen poistaa \naloitetun kierroksen tiedot. \nHaluatko varmasti vaihtaa rataa?")
            if roundStarted == "yes":
                throws = [[] for _ in range(numOfHoles)]
        else:
            throws = [[] for _ in range(numOfHoles)]

    if x == 4:
        course = "Pikkarala-Pro"
        numOfHoles = 18
        holes = list(range(1, numOfHoles + 1))
        length = ["87m", "190m", "215m", "140m", "107m", "191m", "161m", "133m", "166m", "160m", "160m", "289m", "89m", "233m", "194m", "138m", "82m", "155m"]
        par = [3, 4, 4, 4, 3, 4, 3, 3, 3, 4, 4, 5, 3, 5, 4, 3, 3, 3]
        courseFolder = "Courses/Pikkarala"
        courseMapImage = PhotoImage(file=courseFolder + "/coursemap.png")
        courseMapLabel.config(image=courseMapImage)
        holePhotosAvailable = 0
        if any(throws):
            roundStarted = tkinter.messagebox.askquestion("Radan vaihto", "Radan vaihtaminen poistaa \naloitetun kierroksen tiedot. \nHaluatko varmasti vaihtaa rataa?")
            if roundStarted == "yes":
                throws = [[] for _ in range(numOfHoles)]
        else:
            throws = [[] for _ in range(numOfHoles)]

    if x == 5:
        course = "Pikkarala-Ama"
        numOfHoles = 18
        holes = list(range(1, numOfHoles + 1))
        length = ["87m", "112m", "112m", "140m", "107m", "102m", "124m", "90m", "107m", "160m", "93m", "172m", "89m", "138m", "101m", "138m", "82m", "91m"]
        par = [3, 3, 3, 4, 3, 3, 3, 3, 3, 4, 3, 4, 3, 4, 3, 3, 3, 3]
        courseFolder = "Courses/Pikkarala"
        courseMapImage = PhotoImage(file=courseFolder + "/coursemap.png")
        courseMapLabel.config(image=courseMapImage)
        holePhotosAvailable = 0
        if any(throws):
            roundStarted = tkinter.messagebox.askquestion("Radan vaihto", "Radan vaihtaminen poistaa \naloitetun kierroksen tiedot. \nHaluatko varmasti vaihtaa rataa?")
            if roundStarted == "yes":
                throws = [[] for _ in range(numOfHoles)]
        else:
            throws = [[] for _ in range(numOfHoles)]

    if x == 6:
        course = "Köykkyri"
        numOfHoles = 18
        holes = list(range(1, numOfHoles + 1))
        length = ["88m", "126m", "114m", "64m", "102m", "91m", "224m", "89m", "120m", "80m", "95m", "134m", "80m", "109m", "55m", "107m", "196m", "89m"]
        par = [3, 3, 3, 3, 3, 3, 5, 3, 3, 3, 3, 3, 3, 3, 3, 3, 5, 3]
        courseFolder = "Courses/Köykkyri"
        courseMapImage = PhotoImage(file=courseFolder + "/coursemap.png")
        courseMapLabel.config(image=courseMapImage)
        holePhotosAvailable = 0
        if any(throws):
            roundStarted = tkinter.messagebox.askquestion("Radan vaihto", "Radan vaihtaminen poistaa \naloitetun kierroksen tiedot. \nHaluatko varmasti vaihtaa rataa?")
            if roundStarted == "yes":
                throws = [[] for _ in range(numOfHoles)]
        else:
            throws = [[] for _ in range(numOfHoles)]

    if x == 7:
        course = "Honkimaa"
        numOfHoles = 18
        holes = list(range(1, numOfHoles + 1))
        length = ["178m", "98m", "91m", "133m", "65m", "115m", "65m", "210m", "135m", "78m", "116m", "87m", "64m", "131m", "145m", "123m", "74m", "43m"]
        par = [4, 3, 3, 4, 3, 3, 3, 4, 4, 3, 3, 3, 3, 3, 4, 3, 3, 3]
        courseFolder = "Courses/Honkimaa"
        courseMapImage = PhotoImage(file=courseFolder + "/coursemap.png")
        courseMapLabel.config(image=courseMapImage)
        holePhotosAvailable = 0
        if any(throws):
            roundStarted = tkinter.messagebox.askquestion("Radan vaihto", "Radan vaihtaminen poistaa \naloitetun kierroksen tiedot. \nHaluatko varmasti vaihtaa rataa?")
            if roundStarted == "yes":
                throws = [[] for _ in range(numOfHoles)]
        else:
            throws = [[] for _ in range(numOfHoles)]

    if x == 8:
        course = "Muhos"
        numOfHoles = 18
        holes = list(range(1, numOfHoles + 1))
        length = ["71m", "109m", "119m", "60m", "121m", "95m", "71m", "87m", "78m", "109m", "58m", "97m", "70m", "114m", "120m", "110m", "170m", "60m"]
        par = [3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 4, 3, 4, 3]
        courseFolder = "Courses/Muhos"
        courseMapImage = PhotoImage(file=courseFolder + "/coursemap.png")
        courseMapLabel.config(image=courseMapImage)
        holePhotosAvailable = 0
        if any(throws):
            roundStarted = tkinter.messagebox.askquestion("Radan vaihto", "Radan vaihtaminen poistaa \naloitetun kierroksen tiedot. \nHaluatko varmasti vaihtaa rataa?")
            if roundStarted == "yes":
                throws = [[] for _ in range(numOfHoles)]
        else:
            throws = [[] for _ in range(numOfHoles)]

    if x == 9:
        course = "Julkujärvi"
        numOfHoles = 18
        holes = list(range(1, numOfHoles + 1))
        length = ["95m", "126m", "120m", "210m", "192m", "120m", "165m", "82m", "214m", "193m", "128m", "80m", "180m", "111m", "108m", "132m", "256m", "125m"]
        par = [3, 3, 3, 4, 4, 3, 4, 3, 4, 4, 3, 3, 4, 3, 3, 3, 5, 3]
        courseFolder = "Courses/Julkujärvi"
        courseMapImage = PhotoImage(file=courseFolder + "/coursemap.png")
        courseMapLabel.config(image=courseMapImage)
        holePhotosAvailable = 1
        if any(throws):
            roundStarted = tkinter.messagebox.askquestion("Radan vaihto", "Radan vaihtaminen poistaa \naloitetun kierroksen tiedot. \nHaluatko varmasti vaihtaa rataa?")
            if roundStarted == "yes":
                throws = [[] for _ in range(numOfHoles)]
        else:
            throws = [[] for _ in range(numOfHoles)]

    if x == 10:
        course = "Kaskinen"
        numOfHoles = 9
        holes = list(range(1, numOfHoles + 1))
        length = ["80m", "98m", "73m", "140m", "55m", "80m", "64m", "72m", "66m"]
        par = [3, 3, 3, 3, 3, 3, 3, 3, 3]
        courseFolder = "Courses/Kaskinen"
        courseMapImage = PhotoImage(file=courseFolder + "/coursemap.png")
        courseMapLabel.config(image=courseMapImage)
        holePhotosAvailable = 1
        if any(throws):
            roundStarted = tkinter.messagebox.askquestion("Radan vaihto", "Radan vaihtaminen poistaa \naloitetun kierroksen tiedot. \nHaluatko varmasti vaihtaa rataa?")
            if roundStarted == "yes":
                throws = [[] for _ in range(numOfHoles)]
        else:
            throws = [[] for _ in range(numOfHoles)]

    if x == 11:
        course = "Nokia"
        numOfHoles = 12
        holes = list(range(1, numOfHoles + 1))
        length = ["78m", "116m", "80m", "155m", "100m", "94m", "106m", "73m", "57m", "79m", "64m", "55m"]
        par = [3, 3, 3, 4, 3, 3, 3, 3, 3, 3, 3, 3]
        courseFolder = "Courses/Nokia"
        courseMapImage = PhotoImage(file=courseFolder + "/coursemap.png")
        courseMapLabel.config(image=courseMapImage)
        holePhotosAvailable = 0
        if any(throws):
            roundStarted = tkinter.messagebox.askquestion("Radan vaihto", "Radan vaihtaminen poistaa \naloitetun kierroksen tiedot. \nHaluatko varmasti vaihtaa rataa?")
            if roundStarted == "yes":
                throws = [[] for _ in range(numOfHoles)]
        else:
            throws = [[] for _ in range(numOfHoles)]

    if x == 12:
        course = "Pirkkala"
        numOfHoles = 18
        holes = list(range(1, numOfHoles + 1))
        length = ["50m", "72m", "46m", "57m", "142m", "72m", "86m", "69m", "69m", "92m", "?", "94m", "138m", "87m", "67m", "87m", "119m", "64m"]
        par = [3, 3, 3, 3, 4, 3, 3, 3, 3, 3, 4, 3, 4, 3, 3, 3, 3, 3]
        courseFolder = "Courses/Pirkkala"
        courseMapImage = PhotoImage(file=courseFolder + "/coursemap.png")
        courseMapLabel.config(image=courseMapImage)
        holePhotosAvailable = 0
        if any(throws):
            roundStarted = tkinter.messagebox.askquestion("Radan vaihto", "Radan vaihtaminen poistaa \naloitetun kierroksen tiedot. \nHaluatko varmasti vaihtaa rataa?")
            if roundStarted == "yes":
                throws = [[] for _ in range(numOfHoles)]
        else:
            throws = [[] for _ in range(numOfHoles)]

    if x == 13:
        course = "Levi"
        numOfHoles = 9
        holes = list(range(1, numOfHoles + 1))
        length = ["73m", "60m", "85m", "75m", "84m", "57m", "112m", "80m", "91m"]
        par = [3, 3, 3, 3, 3, 3, 3, 3, 3]
        courseFolder = "Courses/Levi"
        courseMapImage = PhotoImage(file=courseFolder + "/coursemap.png")
        courseMapLabel.config(image=courseMapImage)
        holePhotosAvailable = 0
        if any(throws):
            roundStarted = tkinter.messagebox.askquestion("Radan vaihto", "Radan vaihtaminen poistaa \naloitetun kierroksen tiedot. \nHaluatko varmasti vaihtaa rataa?")
            if roundStarted == "yes":
                throws = [[] for _ in range(numOfHoles)]
        else:
            throws = [[] for _ in range(numOfHoles)]

dateAndTime = time.strftime("%d%m%Y-%H%M.txt")



scoreForHoles = [0]*numOfHoles
holeScoresToPar = [0]*numOfHoles
totalScoreToPar = "(E)"
currentHole = 0
totalBUE = [0]*numOfHoles
totalGH = [0]*numOfHoles
totalOCP = [0]*numOfHoles
goodICP = [0]*numOfHoles
badICP = [0]*numOfHoles
totalIBP = [0]*numOfHoles
penalties = [0]*numOfHoles
totalICP = [0]*numOfHoles


### Kuvat painikkeita varten
basketImage = PhotoImage(file="Images/basket.png")
greenImage = PhotoImage(file="Images/green.png")
bueImage = PhotoImage(file="Images/bue.png")
fairwayImage = PhotoImage(file="Images/fairway.png")
obImage = PhotoImage(file="Images/ob.png")
previousImage = PhotoImage(file="Images/previous.png")
nextImage = PhotoImage(file="Images/next.png")
holePhoto = PhotoImage(file="Courses/selectCourse.png")


##########   Radan valinta   ##########
labelForCourseSelection = Label(root)
#   Columnconfigure   #
labelForCourseSelection.columnconfigure(0, weight=1)
labelForCourseSelection.columnconfigure(1, weight=1)
#   Sijoitus   #
labelForCourseSelection.grid(row=0, sticky=E+W)

###   Painikkeet radan valintaa varten   ###
quitButton_CourseSelect = Button(labelForCourseSelection, text="Lopeta", height=2, width=2, bg="#cd0f0f", activebackground="#af0000")
courseButton = Menubutton(labelForCourseSelection, text="Valitse rata", height=2, bg="#787878", activebackground="#5a5a5a", fg="white", activeforeground="white")
courseMenu = Menu(courseButton, tearoff=False)
courseButton.configure(menu=courseMenu)
startRoundButton = Button(labelForCourseSelection, text="Aloita kierros", height=2, width=2, bg="#ffffff", activebackground="#ffffff")
#   Painikkeiden Sijoitus   #
quitButton_CourseSelect.grid(row=0, column=0, sticky=E+W)
startRoundButton.grid(row=0, column=1, sticky=E+W)
courseButton.grid(row=1, columnspan=2, sticky=E+W)

###   Ratakartta   ###
courseMapLabel = Label(labelForCourseSelection, image=courseMapImage)
#   Sijoitus   #
courseMapLabel.grid(row=2, columnspan=2)
##########   -------------   ##########



##########   Tuloskirjaus   ##########
labelForControlButtons_scoring = Label(root)
labelForScoreInfo = Label(root)
labelForNavigation = Label(root)
labelForInputButtons = Label(root)
emptyRow_scoring = Label(root)
#   Columnconfirure   #
labelForControlButtons_scoring.columnconfigure(0, weight=1)
labelForControlButtons_scoring.columnconfigure(1, weight=1)
labelForScoreInfo.columnconfigure(0, weight=1)
labelForScoreInfo.columnconfigure(1, weight=1)
labelForScoreInfo.columnconfigure(2, weight=1)
labelForNavigation.columnconfigure(0, weight=1)
labelForNavigation.columnconfigure(1, weight=1)
labelForInputButtons.columnconfigure(0, weight=1)
labelForInputButtons.columnconfigure(1, weight=1)
labelForInputButtons.columnconfigure(2, weight=1)
emptyRow_scoring.rowconfigure(0, weight=1)
#   Sijoitus   #
labelForControlButtons_scoring.grid(row=0, sticky=E+W)
labelForScoreInfo.grid(row=1, sticky=E+W)
emptyRow_scoring.grid(row=2, sticky=S+N)
labelForNavigation.grid(row=3, sticky=E+W)
labelForInputButtons.grid(row=4, sticky=E+W)

###   Tulouskirjauksen painikkeet ja tietoruudut   ###
##   ControlButtons   ###
quitButton_scoring = Button(labelForControlButtons_scoring, text="Lopeta", height=2, width=2, bg="#cd0f0f", activebackground="#af0000")
eraseButton = Button(labelForControlButtons_scoring, text="Poista", height=2, width=2, bg="#ffffff", activebackground="#ffffff")
couseSelectMenuButton = Button(labelForControlButtons_scoring, text="Radan valinta", height=2, width=2, bg="#787878", activebackground="#5a5a5a", fg="white", activeforeground="white")
summaryLayoutButton = Button(labelForControlButtons_scoring, text="Tulokset", height=2, width=2, bg="#787878", activebackground="#5a5a5a", fg="white", activeforeground="white")
#   Sijoitus   #
quitButton_scoring.grid(row=0, column=0, sticky=E+W)
eraseButton.grid(row=0, column=1, sticky=E+W)
couseSelectMenuButton.grid(row=1, column=0, sticky=E+W)
summaryLayoutButton.grid(row=1, column=1, sticky=E+W)

###   ScoreInfo   ###
holeLabel_Header = Label(labelForScoreInfo, text="Väylä", width=2)
parLabel_Header = Label(labelForScoreInfo, text="Par", width=2)
lengthLabel_Header = Label(labelForScoreInfo, text="Pituus", width=2)
holeLabel = Label(labelForScoreInfo, text="", width=2)
parLabel = Label(labelForScoreInfo, text="", width=2)
lengthLabel = Label(labelForScoreInfo, text="", width=2)
holeLabel.config(text=(currentHole+1, "/", numOfHoles))
parLabel.config(text=(par[currentHole]))
lengthLabel.config(text=(length[currentHole]))
scoreLabel_Header = Label(labelForScoreInfo, text="Väylän Tulos", width=2)
totalScoreLabel_Header = Label(labelForScoreInfo, text="Yhteistulos", width=2)
scoreLabel = Label(labelForScoreInfo, text="0", width=2)
totalScoreLabel = Label(labelForScoreInfo, text="0", width=2)
totalScoreLabel.config(text=(sum(scoreForHoles), totalScoreToPar))
throwsOnHoleLabel = Label(labelForScoreInfo, text="", bd=2, relief="sunken", anchor=W)
holePhotoLabel = Label(labelForScoreInfo)
#   Sijoitus   #
holeLabel_Header.grid(row=0, column=0, sticky=E+W)
parLabel_Header.grid(row=0, column=1, sticky=E+W)
lengthLabel_Header.grid(row=0, column=2, sticky=E+W)
holeLabel.grid(row=1, column=0, sticky=E+W)
parLabel.grid(row=1, column=1, sticky=E+W)
lengthLabel.grid(row=1, column=2, sticky=E+W)
scoreLabel_Header.grid(row=2, column=0, sticky=E+W)
totalScoreLabel_Header.grid(row=2, column=2, sticky=E+W)
scoreLabel.grid(row=3, column=0, sticky=E+W)
totalScoreLabel.grid(row=3, column=2, sticky=E+W)
throwsOnHoleLabel.grid(row=4, columnspan=3, sticky=E+W)
holePhotoLabel.grid(row=5, columnspan=3, sticky=E+W)

###   Navigation   ###
previousHoleButton = Button(labelForNavigation, image=previousImage, bg="#3c3c3c", activebackground="#3c3c3c")
nextHoleButton = Button(labelForNavigation, image=nextImage, bg="#3c3c3c", activebackground="#3c3c3c")
#   Sijoitus   #
previousHoleButton.grid(row=0, column=0, sticky=E+W)
nextHoleButton.grid(row=0, column=1, sticky=E+W)

###   InputButtons   ###
basketButton = Button(labelForInputButtons, image=basketImage, bg="#787878", activebackground="#5a5a5a")
fairwayButton = Button(labelForInputButtons, image=fairwayImage, bg="#787878", activebackground="#5a5a5a")
obButton = Button(labelForInputButtons, image=obImage, bg="#787878", activebackground="#5a5a5a")
greenButton = Button(labelForInputButtons, image=greenImage, bg="#787878", activebackground="#5a5a5a")
bueButton = Button(labelForInputButtons, image=bueImage, bg="#787878", activebackground="#5a5a5a")
otherButton = Menubutton(labelForInputButtons, text="Muut", bg="#787878", activebackground="#5a5a5a", fg="white", activeforeground="white")
otherMenu = Menu(otherButton, tearoff=False)
otherButton.configure(menu=otherMenu)
#   Sijoitus   #
basketButton.grid(row=0, column=2, sticky=E+W)
fairwayButton.grid(row=0, column=1, sticky=E+W)
obButton.grid(row=1, column=0, sticky=E+W)
greenButton.grid(row=1, column=1, sticky=E+W)
bueButton.grid(row=1, column=2, sticky=E+W)
otherButton.grid(row=0, column=0, sticky=E+W+S+N)
##########   ------------   ##########



##########   Tulosnäkymä   ##########
labelForControlButtons_summary = Label(root)
labelForScores = Label(root, borderwidth=1, relief="solid")
emptyRow_summary = Label(root)
#   Columnconfigure   #
labelForControlButtons_summary.columnconfigure(0, weight=1)
labelForControlButtons_summary.columnconfigure(1, weight=1)
labelForScores.columnconfigure(0, weight=1)
labelForScores.columnconfigure(1, weight=1)
labelForScores.columnconfigure(2, weight=1)
emptyRow_summary.rowconfigure(0, weight=1)
#   Sijoitus   #
labelForControlButtons_summary.grid(row=0, sticky=E+W)
emptyRow_summary.grid(row=2, sticky=N+S)
labelForScores.grid(row=3, sticky=E+W+S)


###   ControlButtons_summary   ###
quitButton_summary = Button(labelForControlButtons_summary, text="Lopeta", height=2, width=2, bg="#cd0f0f", activebackground="#af0000")
scoringLayoutButton = Button(labelForControlButtons_summary, text="Kirjaaminen", height=2, width=2, bg="#ffffff", activebackground="#ffffff")
restoreBackupButton =  Button(labelForControlButtons_summary, text="Palauta varmuuskopiosta", height=2, bg="#787878", activebackground="#5a5a5a", fg="white", activeforeground="white")
#   Sijoitus   #
quitButton_summary.grid(row=0, column=0, sticky=E+W)
scoringLayoutButton.grid(row=0, column=1, sticky=E+W)
restoreBackupButton.grid(row=1, columnspan=2, sticky=E+W)

###   Scores   ###
scoreSummaryLabel_Header = Label(labelForScores, text="Tulosten yhteenveto")
totalScoreSum_Header = Label(labelForScores, text="Summa", borderwidth=1, relief="raised", bg="#787878", fg="white")
totalScoreSumToPar_Header = Label(labelForScores, text="To Par", borderwidth=1, relief="raised", bg="#787878", fg="white")
scoreResult_Header = Label(labelForScores, text="Tulos", borderwidth=1, relief="raised", bg="#787878", fg="white")
scoreResult = Label(labelForScores, text="", borderwidth=1, relief="sunken", bg="white")
scoreResultToPar = Label(labelForScores, text="", borderwidth=1, relief="sunken", bg="white")
#   Sijoitus   #
scoreSummaryLabel_Header.grid(row=0, columnspan=3, pady=(20, 20), sticky=E+W)
totalScoreSum_Header.grid(row=1, column=1, sticky=E+W)
totalScoreSumToPar_Header.grid(row=1, column=2, sticky=E+W)
scoreResult_Header.grid(row=2, column=0, sticky=E+W)
scoreResult.grid(row=2, column=1, sticky=E+W)
scoreResultToPar.grid(row=2, column=2, sticky=E+W)

###   Stats   ###
totalSum_Header = Label(labelForScores, text="Summa", borderwidth=1, relief="raised", bg="#787878", fg="white")
totalSumAverage_Header = Label(labelForScores, text="Keskiarvo", borderwidth=1, relief="raised", bg="#787878", fg="white")
bue_header = Label(labelForScores, text="BUE", borderwidth=1, relief="raised", bg="#787878", fg="white")
gh_header = Label(labelForScores, text="GH", borderwidth=1, relief="raised", bg="#787878", fg="white")
ocp_header = Label(labelForScores, text="OCP", borderwidth=1, relief="raised", bg="#787878", fg="white")
icp_header = Label(labelForScores, text="ICP", borderwidth=1, relief="raised", bg="#787878", fg="white")
ibp_header = Label(labelForScores, text="IBP", borderwidth=1, relief="raised", bg="#787878", fg="white")
ob_header = Label(labelForScores, text="OB", borderwidth=1, relief="raised", bg="#787878", fg="white")
bueLabel = Label(labelForScores, text="", borderwidth=1, relief="sunken", bg="white")
ghLabel = Label(labelForScores, text="", borderwidth=1, relief="sunken", bg="white")
ocpLabel = Label(labelForScores, text="", borderwidth=1, relief="sunken", bg="white")
icpLabel = Label(labelForScores, text="", borderwidth=1, relief="sunken", bg="white")
ibpLabel = Label(labelForScores, text="", borderwidth=1, relief="sunken", bg="white")
obLabel = Label(labelForScores, text="", borderwidth=1, relief="sunken", bg="white")
bueLabelAverage = Label(labelForScores, text="", borderwidth=1, relief="sunken", bg="white")
ghLabelAverage = Label(labelForScores, text="", borderwidth=1, relief="sunken", bg="white")
icpLabelAverage = Label(labelForScores, text="", borderwidth=1, relief="sunken", bg="white")
emptyBox1 = Label(labelForScores, borderwidth=1, relief="sunken", bg="white")
emptyBox2 = Label(labelForScores, borderwidth=1, relief="sunken", bg="white")
emptyBox3 = Label(labelForScores, borderwidth=1, relief="sunken", bg="white")
#   Sijoitus   #
totalSum_Header.grid(row=3, column=1, sticky=E+W, pady=(50, 0))
totalSumAverage_Header.grid(row=3, column=2, sticky=E+W, pady=(50, 0))
bue_header.grid(row=4, column=0, sticky=E+W)
gh_header.grid(row=5, column=0, sticky=E+W)
ocp_header.grid(row=6, column=0, sticky=E+W)
icp_header.grid(row=7, column=0, sticky=E+W)
ibp_header.grid(row=8, column=0, sticky=E+W)
ob_header.grid(row=9, column=0, sticky=E+W)
bueLabel.grid(row=4, column=1, sticky=E+W)
ghLabel.grid(row=5, column=1, sticky=E+W)
ocpLabel.grid(row=6, column=1, sticky=E+W)
icpLabel.grid(row=7, column=1, sticky=E+W)
ibpLabel.grid(row=8, column=1, sticky=E+W)
obLabel.grid(row=9, column=1, sticky=E+W)
bueLabelAverage.grid(row=4, column=2, sticky=E+W)
ghLabelAverage.grid(row=5, column=2, sticky=E+W)
icpLabelAverage.grid(row=7, column=2, sticky=E+W)
emptyBox1.grid(row=6, column=2, sticky=E+W)
emptyBox2.grid(row=8, column=2, sticky=E+W)
emptyBox3.grid(row=9, column=2, sticky=E+W)



def hideScoringLayout():
    labelForControlButtons_scoring.grid_remove()
    labelForScoreInfo.grid_remove()
    labelForNavigation.grid_remove()
    labelForInputButtons.grid_remove()
    emptyRow_scoring.grid_remove()

def showScoringLayout():
    labelForControlButtons_scoring.grid()
    labelForScoreInfo.grid()
    labelForNavigation.grid()
    labelForInputButtons.grid()
    emptyRow_scoring.grid()

def hideCourseSelectionLayout():
    labelForCourseSelection.grid_remove()

def showCourseSelectionLayout():
    labelForCourseSelection.grid()

def hideSummaryLayout():
    labelForControlButtons_summary.grid_remove()
    labelForScores.grid_remove()
    emptyRow_summary.grid_remove()

def showSummaryLayout():
    labelForControlButtons_summary.grid()
    labelForScores.grid()
    emptyRow_summary.grid()

def goToScoringLayout():
    global holePhotosAvailable, holePhoto, holePhotoLabel, currentHole
    hideCourseSelectionLayout()
    showScoringLayout()
    hideSummaryLayout()
    holeLabel.config(text=(currentHole + 1, "/", numOfHoles))
    parLabel.config(text=(par[currentHole]))
    lengthLabel.config(text=(length[currentHole]))
    if holePhotosAvailable == 1:
        holePhoto = PhotoImage(file=courseFolder + "/" + str((currentHole+1)) + ".png")
        holePhotoLabel.config(image=holePhoto)

def goToCourseSelection():
    hideScoringLayout()
    hideSummaryLayout()
    showCourseSelectionLayout()

def goToSummaryLayout():
    global totalScoreToPar, numOfHoles
    hideScoringLayout()
    hideCourseSelectionLayout()
    scoreResult.config(text = sum(scoreForHoles))
    scoreResultToPar.config(text=totalScoreToPar)
    bueLabel.config(text=sum(totalBUE))
    ghLabel.config(text=sum(totalGH))
    ocpLabel.config(text=sum(totalOCP))
    icpLabel.config(text=(sum(goodICP), "/", (sum(goodICP)+sum(badICP))))
    ibpLabel.config(text=sum(totalIBP))
    obLabel.config(text=sum(penalties))
    if numOfHoles != 0:
        bueLabelAverage.config(text=(round((sum(totalBUE)*100/numOfHoles), 2), "%"))
        ghLabelAverage.config(text=(round((sum(totalGH) * 100 / numOfHoles), 2), "%"))
    else:
        bueLabelAverage.config(text="0 %")
        ghLabelAverage.config(text="0 %")
    if (sum(goodICP)+sum(badICP)) != 0:
        icpLabelAverage.config(text=(round((sum(goodICP)*100/(sum(goodICP)+sum(badICP))), 2), "%"))
    else:
        icpLabelAverage.config(text="0 %")
    showSummaryLayout()



goToCourseSelection()



### Funktiot painikkeita varten

def makeBackup():
    global throws, numOfHoles
    backupFileName = str("Backup/backup.txt")
    backupFile = open(backupFileName, "w")

    for i in range(0, numOfHoles):
        for a in range(0, len(throws[i])):
            backupFile.write(str(throws[i][a]))
        backupFile.write("\n")

    backupFile.close()

def restoreBackup():
    restore = tkinter.messagebox.askquestion("Palauta varmuuskopiosta", "Haluatko varmasti \npalauttaa kierroksen \ntiedot varmuuskopiosta?\nTämän kierroksen \ntiedot menetetään.")
    if restore == "yes":
        global throws, numOfHoles
        backupFileName = str("Backup/backup.txt")
        backupFile = open(backupFileName, "r")

        backupOfThrows = ([line.rstrip() for line in backupFile])
        backupFile.close()
        throws = [[] for _ in range(numOfHoles)]

        for i in range(0, numOfHoles):
            for a in range(0, (len(backupOfThrows[i]))):
                throws[i].append(backupOfThrows[i][a])


def countTotalScore():
    global currentHole, numOfHoles, totalGH, totalBUE, totalOCP, totalIBP, goodICP, badICP, penalties, scoreForHoles, totalScoreToPar, par, holeScoresToPar, totalICP, course
    scoreForHoles = [0]*numOfHoles
    holeScoresToPar = [0]*numOfHoles
    totalBUE = [0]*numOfHoles
    totalGH = [0]*numOfHoles
    totalOCP = [0]*numOfHoles
    goodICP = [0]*numOfHoles
    badICP = [0]*numOfHoles
    totalIBP = [0]*numOfHoles
    penalties = [0]*numOfHoles
    totalICP = [0]*numOfHoles
    if course != "Esimerkkirata":
        tmpFileName = str("Tulokset/" + course + " " + dateAndTime)
        tmpFile = open(tmpFileName, "w")

    def countScore(x):
        global numOfHoles, scoreForHoles

        ## Laske penaltyt
        for i in range(0, len(throws[x])):
            if throws[x][i] == "o" or throws[x][i] == "G" or throws[x][i] == "B" or throws[x][i] == "K":
                penalties[x] += 1

        ## Laske väylien tulokset
        scoreForHoles[x] = (len(throws[x]) + penalties[x])

        ## Laske GH ja BUE
        numOfThrows = 0
        ghOrNot = 0
        while numOfThrows < len(throws[x]):
            if throws[x][ghOrNot] == "o" or throws[x][ghOrNot] == "G" or throws[x][ghOrNot] == "B" or throws[x][
                ghOrNot] == "K":
                numOfThrows += 2
            else:
                numOfThrows += 1
            if throws[x][ghOrNot] == "g" or throws[x][ghOrNot] == "G" or throws[x][ghOrNot] == "b" or throws[x][
                ghOrNot] == "B" or throws[x][ghOrNot] == "k" or throws[x][ghOrNot] == "K":
                if numOfThrows <= (par[x] - 2):
                    totalGH[x] = 1
            if throws[x][ghOrNot] == "b" or throws[x][ghOrNot] == "B" or throws[x][ghOrNot] == "k" or throws[x][
                ghOrNot] == "K":
                if numOfThrows <= (par[x] - 2):
                    totalBUE[x] = 1
            ghOrNot += 1

        ## Laske ICP, OCP ja IBP
        if len(throws[x]) != 0:
            if (throws[x][len(throws[x]) - 1]) == "k" or (throws[x][len(throws[x]) - 1]) == "K":
                if (throws[x][len(throws[x]) - 2]) == "b" or (throws[x][len(throws[x]) - 2]) == "B":
                    totalIBP[x] = 1
                elif (throws[x][len(throws[x]) - 2]) == "g" or (throws[x][len(throws[x]) - 2]) == "G":
                    goodICP[x] = 1
                else:
                    totalOCP[x] = 1
            badICPcount = 0
            if throws[x][len(throws[x])-1] == "k" or throws[x][len(throws[x])-1] == "K":
                while badICPcount < len(throws[x]):
                    if throws[x][badICPcount] == "g" or throws[x][badICPcount] == "G" or throws[x][badICPcount] == "b" or \
                            throws[x][badICPcount] == "B":
                        if throws[x][badICPcount + 1] != "k" and throws[x][badICPcount + 1] != "K":
                            badICP[x] += 1
                    badICPcount += 1
        totalICP[x] = goodICP[x] + badICP[x]

    for i in range(0, numOfHoles):
        countScore(i)
    for i in range(0, numOfHoles):
        if len(throws[i]) != 0:
            if throws[i][len(throws[i])-1] == "k" or throws[i][len(throws[i])-1] == "K":
                holeScoresToPar[i] = (scoreForHoles[i] - par[i])
    if sum(holeScoresToPar) == 0:
        totalScoreToPar = "(E)"
    elif sum(holeScoresToPar) > 0:
        totalScoreToPar = "(+" + str(sum(holeScoresToPar)) + ")"
    else:
        totalScoreToPar = "(" + str(sum(holeScoresToPar)) + ")"
    scoreLabel.config(text=(scoreForHoles[currentHole]))
    throwsOnHoleLabel.config(text=throws[currentHole])

    if course != "Esimerkkirata":
        scoreToFile = '\t'.join(str(e) for e in scoreForHoles) + "\n"
        bueToFile = '\t'.join(str(e) for e in totalBUE) + "\n"
        ghToFile = '\t'.join(str(e) for e in totalGH) + "\n"
        ocpToFile = '\t'.join(str(e) for e in totalOCP) + "\n"
        icpToFile = '\t'.join(str(e) for e in totalICP) + "\n"
        ibpToFile = '\t'.join(str(e) for e in totalIBP) + "\n"
        obToFile = '\t'.join(str(e) for e in penalties) + "\n"
        tmpFile.write(scoreToFile)
        tmpFile.write(bueToFile)
        tmpFile.write(ghToFile)
        tmpFile.write(ocpToFile)
        tmpFile.write(icpToFile)
        tmpFile.write(ibpToFile)
        tmpFile.write(obToFile)
        tmpFile.close()



def inBasket():
    global currentHole
    if len(throws[currentHole]) == 0:
        throws[currentHole].append("k")
    elif throws[currentHole][len(throws[currentHole])-1] != "k" and throws[currentHole][len(throws[currentHole])-1] != "K":
        throws[currentHole].append("k")
    countTotalScore()
    totalScoreLabel.config(text=(sum(scoreForHoles), totalScoreToPar))

def inFairway():
    global currentHole
    if len(throws[currentHole]) == 0:
        throws[currentHole].append("v")
    elif throws[currentHole][len(throws[currentHole])-1] != "k" and throws[currentHole][len(throws[currentHole])-1] != "K":
        throws[currentHole].append("v")
    countTotalScore()

def inOB():
    global currentHole
    if len(throws[currentHole]) == 0:
        throws[currentHole].append("o")
    elif throws[currentHole][len(throws[currentHole])-1] != "k" and throws[currentHole][len(throws[currentHole])-1] != "K":
        throws[currentHole].append("o")
    countTotalScore()

def inGreen():
    global currentHole
    if len(throws[currentHole]) == 0:
        throws[currentHole].append("g")
    elif throws[currentHole][len(throws[currentHole])-1] != "k" and throws[currentHole][len(throws[currentHole])-1] != "K":
        throws[currentHole].append("g")
    countTotalScore()

def inBUE():
    global currentHole
    if len(throws[currentHole]) == 0:
        throws[currentHole].append("b")
    elif throws[currentHole][len(throws[currentHole])-1] != "k" and throws[currentHole][len(throws[currentHole])-1] != "K":
        throws[currentHole].append("b")
    countTotalScore()

def inGreen_P():
    global currentHole
    if len(throws[currentHole]) == 0:
        throws[currentHole].append("G")
    elif throws[currentHole][len(throws[currentHole])-1] != "k" and throws[currentHole][len(throws[currentHole])-1] != "K":
        throws[currentHole].append("G")
    countTotalScore()

def inBUE_P():
    global currentHole
    if len(throws[currentHole]) == 0:
        throws[currentHole].append("B")
    elif throws[currentHole][len(throws[currentHole])-1] != "k" and throws[currentHole][len(throws[currentHole])-1] != "K":
        throws[currentHole].append("B")
    countTotalScore()

def inBasket_P():
    global currentHole
    if len(throws[currentHole]) == 0:
        throws[currentHole].append("K")
    elif throws[currentHole][len(throws[currentHole])-1] != "k" and throws[currentHole][len(throws[currentHole])-1] != "K":
        throws[currentHole].append("K")
    countTotalScore()
    totalScoreLabel.config(text=(sum(scoreForHoles), totalScoreToPar))

def previousHole():
    global currentHole, numOfHoles, courseFolder, holePhotoLabel, holePhoto, holePhotosAvailable, scoreForHoles
    if currentHole > 0:
        currentHole -= 1
    elif currentHole == 0:
        currentHole = numOfHoles-1
    totalScoreLabel.config(text=(sum(scoreForHoles), totalScoreToPar))
    holeLabel.config(text=(currentHole+1, "/", numOfHoles))
    parLabel.config(text=(par[currentHole]))
    lengthLabel.config(text=(length[currentHole]))
    if holePhotosAvailable == 1:
        holePhoto = PhotoImage(file=courseFolder + "/" + str((currentHole+1)) + ".png")
        holePhotoLabel.config(image=holePhoto)
    countTotalScore()
    if sum(scoreForHoles) > 3:
        makeBackup()

def nextHole():
    global currentHole, numOfHoles, courseFolder, holePhotoLabel, holePhoto, holePhotosAvailable, scoreForHoles
    if currentHole < numOfHoles-1:
        currentHole += 1
    elif currentHole == numOfHoles-1:
        currentHole = 0
    totalScoreLabel.config(text=(sum(scoreForHoles), totalScoreToPar))
    holeLabel.config(text=(currentHole+1, "/", numOfHoles))
    parLabel.config(text=(par[currentHole]))
    lengthLabel.config(text=(length[currentHole]))
    if holePhotosAvailable == 1:
        holePhoto = PhotoImage(file=courseFolder + "/" + str((currentHole+1)) + ".png")
        holePhotoLabel.config(image=holePhoto)
    countTotalScore()
    if sum(scoreForHoles) > 3:
        makeBackup()

def eraseThrow():
    global currentHole
    if len(throws[currentHole]) != 0:
        throws[currentHole].pop()
    countTotalScore()

def quitRound():
    endRound = tkinter.messagebox.askquestion("Lopeta kierros", "Haluatko varmasti \nlopettaa kierroksen?")
    if endRound == "yes":
        root.destroy()
###


##### Funktioiden liittäminen painikkeisiin
# Tuloskirjaus
quitButton_scoring.config(command=quitRound)
couseSelectMenuButton.config(command=goToCourseSelection)
eraseButton.config(command=eraseThrow)
previousHoleButton.config(command=previousHole)
summaryLayoutButton.config(command=goToSummaryLayout)
nextHoleButton.config(command=nextHole)
holeLabel.config(text=(currentHole+1, "/", numOfHoles))
parLabel.config(text=(par[currentHole]))
lengthLabel.config(text=(length[currentHole]))
totalScoreLabel.config(text=(sum(scoreForHoles), totalScoreToPar))
basketButton.config(command=inBasket)
fairwayButton.config(command=inFairway)
obButton.config(command=inOB)
greenButton.config(command=inGreen)
bueButton.config(command=inBUE)
otherMenu.add_command(label="Green (pen)", command=inGreen_P)
otherMenu.add_command(label="BUE (pen)", command=inBUE_P)
otherMenu.add_command(label="Kori (pen)", command=inBasket_P)

# Radan valinta
quitButton_CourseSelect.config(command=quitRound)
courseMenu.add_radiobutton(label="Meri-Toppila", command=lambda: selectedCourse(1))
courseMenu.add_radiobutton(label="Virpiniemi", command=lambda: selectedCourse(2))
courseMenu.add_radiobutton(label="Hiironen", command=lambda: selectedCourse(3))
courseMenu.add_radiobutton(label="Pikkarala-Pro", command=lambda: selectedCourse(4))
courseMenu.add_radiobutton(label="Pikkarala-Ama", command=lambda: selectedCourse(5))
courseMenu.add_radiobutton(label="Köykkyri", command=lambda: selectedCourse(6))
courseMenu.add_radiobutton(label="Honkimaa", command=lambda: selectedCourse(7))
courseMenu.add_radiobutton(label="Muhos", command=lambda: selectedCourse(8))
courseMenu.add_radiobutton(label="Julkujärvi", command=lambda: selectedCourse(9))
courseMenu.add_radiobutton(label="Kaskinen", command=lambda: selectedCourse(10))
courseMenu.add_radiobutton(label="Nokia", command=lambda: selectedCourse(11))
courseMenu.add_radiobutton(label="Pirkkala", command=lambda: selectedCourse(12))
courseMenu.add_radiobutton(label="Levi", command=lambda: selectedCourse(13))
startRoundButton.config(command=goToScoringLayout)

# Tulosten yhteenveto
quitButton_summary.config(command=quitRound)
scoringLayoutButton.config(command=goToScoringLayout)
restoreBackupButton.configure(command=restoreBackup)
#####



root.mainloop()
